# How to test capstone with Selenium IDE

- Open Selenium IDE from as Google Chrome Extension.

	![Selenium IDE as Extension](/readme-images/s1.png "Selenium IDE as Chrome Extension")

- Open project.

	![Open .side file](/readme-images/s2.png "Selenium IDE project file")

- Add the url to the portfolio

	![Add Url](/readme-images/s3-add.png "Selenium IDE add url")

- Run all test cases.
	![Run Tests](/readme-images/s3-run.png "Selenium IDE test run")